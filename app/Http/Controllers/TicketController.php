<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Zendesk\API\HttpClient as ZendeskAPI;
use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Processor\WebProcessor;
use Monolog\Processor\MemoryUsageProcessor;
use Monolog\Formatter\LineFormatter;

class TicketController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function showOption($ticketID = null)
    {   
        $response = [
            'ivr_ticket'    => $ticketID,
            'zd_ticket'     => null,
            'status'        => null,
            'option'        => null,
            'group'         => null,
            'error_code'    => null,
            'error'         => null
        ];

        if(!is_null($ticketID))
        {
            $ZendeskClient = new ZendeskAPI(env('ZD_SUBDOMAIN'), env('ZD_USERNAME'));
            $ZendeskClient->setAuth('basic', ['username' => env('ZD_USERNAME'), 'token' => env('ZD_TOKEN')]);
            
            try 
            {
                
                //Obtener el ticket $ticketID
                $ticket = $ZendeskClient->tickets()->find($ticketID)->ticket;
                $response['zd_ticket']  = $ticket->id;
                $response['status']     = $ticket->status;
                $response['group']      = $ticket->group_id;

                foreach ($ticket->custom_fields as $field) 
                {
                    if ($field->id == env('ZD_OPTIONID')) 
                    {
                        $response['option'] = $field->value;
                    }
                }

                if(is_null($response['option']) || empty($response['option']))
                {
                    $response['option'] = env('DEFAULT_OPTION');
                    $response['error'] = 'Group unmapped, ticket without option (Default transfer)';
                }

            } catch (\Zendesk\API\Exceptions\ApiResponseException $e) {
                
                //En caso de que haya algun error, se lanzará una excepción $e
                $response['error_code'] = $e->getCode();
                $errorDetails = json_decode($e->getErrorDetails())->error;
                if(isset($errorDetails->message))
                {
                    $response['error'] = $errorDetails->message;
                }else{
                    $response['error'] = $errorDetails;
                }

            }

        }else{

            $response['error'] = 'No ticket id from IVR';
        
        }

        $this->logTicketRequest($response);
        $responseString = '|'.implode('|', $response).'|';
        return $responseString;
    }
    
    public function showOptionJson($ticketID = null)
    {   
        $response = [
            'ivr_ticket'    => $ticketID,
            'zd_ticket'     => null,
            'status'        => null,
            'option'        => null,
            'group'         => null,
            'error_code'    => null,
            'error'         => null
        ];

        if(!is_null($ticketID))
        {
            $ZendeskClient = new ZendeskAPI(env('ZD_SUBDOMAIN'), env('ZD_USERNAME'));
            $ZendeskClient->setAuth('basic', ['username' => env('ZD_USERNAME'), 'token' => env('ZD_TOKEN')]);
            
            try 
            {
                
                //Obtener el ticket $ticketID
                $ticket = $ZendeskClient->tickets()->find($ticketID)->ticket;
                $response['zd_ticket']  = $ticket->id;
                $response['status']     = $ticket->status;
                $response['group']      = $ticket->group_id;

                foreach ($ticket->custom_fields as $field) 
                {
                    if ($field->id == env('ZD_OPTIONID')) 
                    {
                        $response['option'] = $field->value;
                    }
                }

                if(is_null($response['option']) || empty($response['option']))
                {
                    $response['option'] = env('DEFAULT_OPTION');
                    $response['error'] = 'Group unmapped, ticket without option (Default transfer)';
                }

            } catch (\Zendesk\API\Exceptions\ApiResponseException $e) {
                
                //En caso de que haya algun error, se lanzará una excepción $e
                $response['error_code'] = $e->getCode();
                $errorDetails = json_decode($e->getErrorDetails())->error;
                if(isset($errorDetails->message))
                {
                    $response['error'] = $errorDetails->message;
                }else{
                    $response['error'] = $errorDetails;
                }

            }

        }else{

            $response['error'] = 'No ticket id from IVR';
        
        }

        $this->logTicketRequest($response);
        $responseString = json_encode($response);
        return $responseString;
    }

    private function logTicketRequest($response = [])
    {
        $responseString = implode('|', $response);
        $log = new Logger('ticketRequest');
        $handler = new RotatingFileHandler(storage_path().'/logs/ZendeskRequest/ticket_request.log', Logger::INFO);
        $handler->setFormatter(new LineFormatter("[%datetime%] %channel%.%level_name%: %message%\n"));
        $log->pushHandler($handler);
        $log->pushProcessor(new WebProcessor);
        $log->pushProcessor(new MemoryUsageProcessor);
        $log->addInfo($responseString);
    }
}
